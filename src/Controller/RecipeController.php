<?php

namespace App\Controller;

use App\Entity\Mark;
use App\Entity\Recipe;
use App\Form\MarkType;
use App\Form\RecipeType;
use App\Repository\MarkRepository;
use App\Repository\RecipeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class RecipeController extends AbstractController
{
    #[IsGranted('ROLE_USER')]
    #[Route('/recipe', name: 'app_recipe',methods:['GET'])]

    /**
     * Cette methode affiche la liste de recette de l'utilisareur connecté
     *
     * @param PaginatorInterface $paginator
     * @param RecipeRepository $repository
     * @param Request $request
     * @return Response
     */
    public function index(PaginatorInterface $paginator, RecipeRepository $repository, Request $request): Response
    {
            $recipes = $paginator->paginate(
            $repository->findBy(['user'=>$this->getUser()]),
            $request->query->getInt('page', 1),10);
    
        return $this->render('recipe/index.html.twig', [
            'recipes' => $recipes
        ]);
    }



    #[Route('/recipe/public', name:'app_recipe_public', methods:['GET'])]

    /**
     * Cette methode affiche toutes les recettes publiques.
     *
     * @return Response
     */
    public function indexPublic(PaginatorInterface $paginator, RecipeRepository $repository, Request $request):Response
    {
        $recipes = $paginator->paginate($repository->findPublicRecipe(null), $request->query->getInt('page', 1),10);
        return $this->render('recipe/public.html.twig',[
            'recipes'=>$recipes
        ]);
    }



    #[IsGranted('ROLE_USER')]
    #[Route('/recipe/create', name: 'app_recipe_create')]

    /**
     * Cette methode permet de créer une recette
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function new(Request $request, EntityManagerInterface $manager): Response
    {   
        $recipe = new Recipe();
        $form = $this->createForm(RecipeType::class, $recipe);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $recipe = $form->getData();
            $recipe->setUser($this->getUser());
            $manager->persist($recipe);

            $manager->flush();

            $this->addFlash('success', 'Votre recette a été crééé avec succès!');
            
            return $this->redirectToRoute('app_recipe');
        }

        
        return $this->render('recipe/new.html.twig',[
            'form' => $form->createView(),
        ]);
    }
    

    #[Security("is_granted('ROLE_USER') and recipe.getIsPublic() === true || user === recipe.getUser()")]
    #[Route('/recipe/{id}', name: 'app_recipe_show', methods:['GET','POST'])]

    /**
     * Cette methode permet d'afficher une recette specifique
     *
     * @param Recipe $recipe
     * @return Response
     */
    public function show(Recipe $recipe, Request $request, MarkRepository $markRepository, EntityManagerInterface $manager):Response
    {
        $mark = new Mark();
        $form = $this->createForm(MarkType::class,$mark);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mark->setUser($this->getUser())
            ->setRecipe($recipe);

            $existingMark = $markRepository->findOneBy([
                'user' => $this->getUser(), 
                'recipe' => $recipe
            ]);

            if(!$existingMark) {
                
                $manager->persist($mark);
            }
            else{
                
                $existingMark->setMark($form->getData()->getMark());
                
            }

            $manager->flush();

            $this->addFlash('success', 'La recette a été notée avec succès!');

            return $this->redirectToRoute('app_recipe_show', ['id' => $recipe->getId()]);
            
        }

        return $this->render('recipe/show.html.twig',[
            'recipe' => $recipe,
            'form'=>$form->createView()
        
        
        ]);
    }

    
    #[Route('/recipe/edit/{id}', name: 'app_recipe_edit',methods:['GET','POST'])]
    #[Security('is_granted("ROLE_USER") and user === recipe.getUser()')]
    /**
     * Cette methode permet de modifier une recette
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Recipe $recipe
     * @return Response
     */
    public function edit(Request $request , EntityManagerInterface $manager , Recipe $recipe): Response
    {
        $form = $this->createForm(RecipeType::class, $recipe);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $recipe = $form->getData();
            $manager->persist($recipe);
            $manager->flush();

            $this->addFlash('success', 'Votre recette a été modifiée avec succès!');
            return $this->redirectToRoute('app_recipe');
        }

        return $this->render('recipe/edit.html.twig',[
            'form' => $form->createView(),
        ]);

    }

    #[Route('/recipe/delete/{id}', name: 'app_recipe_delete', methods:['GET'])]
    #[Security("is_granted('ROLE_USER') and user === recipe.getUser()")]


    /**
     * Cette methode permet de supprimer une recette
     *
     * @param EntityManagerInterface $manager
     * @param Recipe $recipe
     * @return Response
     */
    public function delete(EntityManagerInterface $manager, Recipe $recipe):Response
    {
        
        $manager->remove($recipe);
        $manager->flush();

        $this->addFlash('success','Votre recette a été supprimée avec succès!');
        
        return $this->redirectToRoute('app_recipe');

    }


}
