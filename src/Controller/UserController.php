<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\UserPasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class UserController extends AbstractController
{
    

    #[Security("is_granted('ROLE_USER') and user === choosenUser")]
    #[Route('/user/{id}', name: 'app_user', methods:['GET','POST'])]
    
    /**
     * Methode pour afficher les informations de l'utilisateur.
     *
     * @param User $choosenUser
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param UserPasswordHasherInterface $hasher
     * @return Response
     */
    public function index(User $choosenUser, Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $hasher): Response
    {
        
        $form = $this->createForm(UserType::class, $choosenUser);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($hasher->isPasswordValid($choosenUser,$form->getData()->getPlainPassword())){

                $choosenUser = $form->getData();
                
                $manager->persist($choosenUser);
                $manager->flush();
    
                $this->addFlash('success','Les informations de votre compte ont été modifiées avec succès!');
    
                return $this->redirectToRoute('app_recipe');
            }else{
                $this->addFlash('warning','Votre mot de passe est incorrect');
            }

        }

        return $this->render('user/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }



    #[Security("is_granted('ROLE_USER') and user === choosenUser")]
    #[Route('/user/edit_password/{id}', name:'app_user_edit_password',methods:['GET','POST'])]
    
    /**
     * Methode pour modifier votre mot de passe
     *
     * @param Request $request
     * @param User $user
     * @param UserPasswordHasherInterface $hasher
     * @param EntityManagerInterface $manager
     * @return Response
     */
    
    
     public function editPassword(Request $request, User $choosenUser, UserPasswordHasherInterface $hasher, EntityManagerInterface $manager):Response
    {
        $form = $this->createForm(UserPasswordType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            if ($hasher->isPasswordValid($choosenUser, $form->getData()['plainPassword'])) {
                
                $choosenUser->setUpdatedAt(new \DateTimeImmutable());

                $choosenUser->setPlainPassword($form->getData()['newPassword']);
                
                $manager->persist($choosenUser);
                
                $manager->flush();

                
                $this->addFlash('success','Votre mot de passe a été modifié avec succès');

                return $this->redirectToRoute('app_recipe');

            }else{
                $this->addFlash('warning','Votre mot de passe est incorrect');
            }
        }
        return $this->render('user/edit_password.html.twig', [
            'form'=>$form->createView()

        ]);
    }
}
