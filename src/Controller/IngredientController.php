<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Form\IngredientType;
use App\Repository\IngredientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class IngredientController extends AbstractController
{
    /***
     * Cette fonction permet de recupérer les ingredients de l'utilisateur connecté
     * 
     * @param IngredientRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    #[Route('/ingredient', name: 'app_ingredient',methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    
    public function index(IngredientRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {
        $ingredients = $paginator->paginate(
            $repository->findBy(['user'=>$this->getUser()]),
            $request->query->getInt('page', 1),10);
    
        return $this->render('ingredient/index.html.twig', [
            'ingredients' => $ingredients,
            
        ]);
    }

    #[Route('/ingredient/nouveau', name: 'app_ingredient_create', methods: ['GET','POST'])]
    #[IsGranted('ROLE_USER')]
    
    /**
     * Cette methode permet de créer un nouveau ingredient
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function new(Request $request, EntityManagerInterface $manager):Response
    {
        $ingredient = new Ingredient();

        $form = $this->createForm(IngredientType::class, $ingredient);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ingredient = $form->getData();
            $ingredient->setUser($this->getUser());
            $manager->persist($ingredient);
            $manager->flush();
            
            $this->addFlash('success','Votre ingrédient a été crée avec succès.');

           return $this->redirectToRoute('app_ingredient');
            
        }
        
        return $this->render('ingredient/new.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    
    
    #[Security("is_granted('ROLE_USER') and user === ingredient.getUser()")]
    #[Route('/ingredient/edit/{id}', name: 'app_ingredient_edit', methods:['GET','POST'])]
    /**
     * Cette methode permet de modifier un ingrédient
     *
     * @param Ingredient $ingredient
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Ingredient $ingredient, Request $request, EntityManagerInterface $manager):Response
    {
    
        $form = $this->createForm(IngredientType::class, $ingredient);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ingredient = $form->getData();
            $manager->persist($ingredient);
            $manager->flush();
            
            $this->addFlash('success','Votre ingrédient a été modifié avec succès.');

           return $this->redirectToRoute('app_ingredient');
        }
        
        return $this->render('ingredient/edit.html.twig',[
            'form'=> $form->createView(),
        ]);
    }


    #[Route('/ingredient/delete/{id}', name:'app_ingredient_delete', methods: ['GET'])]
    #[Security("is_granted('ROLE_USER') and user === ingredient.getUser()")]
    /**
     * Cette methode permet de supprimer un ingrédient
     *
     * @param EntityManagerInterface $manager
     * @param Ingredient $ingredient
     * @return Response
     */
    public function delete(EntityManagerInterface $manager, Ingredient $ingredient): Response
    {
        
        $manager->remove($ingredient);
        $manager->flush();

        $this->addFlash('success','Votre ingrédient a été supprimée.');

        return $this->redirectToRoute('app_ingredient');

    }
}
