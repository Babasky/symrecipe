<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    #[Route('/login', name: 'app_security',methods:['GET','POST'])]

    /**
     * The login action.
     * @param AuthenticationUtils $authenticationUtils
     */

    public function index(AuthenticationUtils $authenticationUtils): Response
    {
        $lastUsername = $authenticationUtils->getLastUsername();
        $error = $authenticationUtils->getLastAuthenticationError();
        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    #[Route('/logout', name:'app_logout', methods:['GET'])]
    /**
     * This method is used to logout.
     *
     * @return void
     */
    public function logout()
    {
        //Nothing to do here
    }


    #[Route('/register', name:'app_register', methods:['GET','POST'])]

    /**
     * This method is used to register a new user.
     * @return \Symfony\Component\HttpFoundation\Response
     * @param Request $request
     * @param EntityManagerInterface $manager
     */
    
    public function registration(Request $request, EntityManagerInterface $manager):Response
    {   
        $user = new User();
        $user->setRoles(['ROLE_USER']);
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success','Inscription reussie');

            return $this->redirectToRoute('app_security');
        }

        return $this->render('security/registration.html.twig',[
            'form'=> $form->createView(),
        ]);
    }
}
