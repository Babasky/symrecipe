<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Mark;
use App\Entity\User;
use Faker\Generator;
use App\Entity\Recipe;
use App\Entity\Contact;
use App\Entity\Ingredient;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /**
     * @var Generator
     */
    private Generator $faker;
    
    
    public function __construct(){
        $this->faker = Factory::create('fr_FR');
    }
    
    
    public function load(ObjectManager $manager): void
    {

         //User
         $users = [];

         $admin = new User();
         $admin->setFullName('Administrateur')
            ->setPseudo(null)
            ->setEmail('admin@symrecipe.com')
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN'])
            ->setPlainPassword('admin');
            $users[] = $admin;
            $manager->persist($admin);

         for($u= 1;  $u<= 10; $u++) {
            $user = new User();
            $user->setFullName($this->faker->name(50))
            ->setPseudo(mt_rand(0,1)==1? $this->faker->firstname(50):null)
            ->setEmail($this->faker->email(50))
            ->setRoles(['ROLE_USER'])
            ->setPlainPassword('password');
        
        

            $user->setPassword('password');
            $users[] = $user;

            $manager->persist($user);
        }


        // Ingredients
        $ingredients = [];
        for($i = 1; $i <= 50; ++$i) {
         $ingredient = new Ingredient();
         $ingredient->setName($this->faker->word())
            ->setPrice(mt_rand(200,500))
            ->setUser($users[mt_rand(0,count($users) - 1)]);
            $ingredients[] = $ingredient;
            $manager->persist($ingredient);
        }

        //Recipe
        $recipes = [];
        for( $j= 1;  $j<= 25; $j++) {
            $recipe = new Recipe();
            $recipe->setName($this->faker->word())
                ->setTime(mt_rand(0,1)== 1 ?mt_rand(1,3600):null)
                ->setNbPeople(mt_rand(0,1)==1 ?mt_rand(1,50):null)
                ->setDifficulty(mt_rand(0,1)==1?mt_rand(1,5):null)
                ->setDescription($this->faker->text(200))
                ->setPrice(mt_rand(0,1)==1?mt_rand(1,1000):null)
                ->setIsFavorite(mt_rand(0,1)==1?true:false)
                ->setIsPublic(mt_rand(0,1)==1?true:false)
                ->setUser($users[mt_rand(0,count($users)-1)]);
                
                for($k = 0; $k < mt_rand(5,15); $k++) {
                    $recipe->addIngredient($ingredients[mt_rand(0,count($ingredients)-1)]);
                }
                $recipes[] = $recipe;
                $manager->persist($recipe);
        
                }



        // Mark
        foreach($recipes as $recipe){
            for($m = 0; $m < mt_rand(0,5); $m++) {
                $mark = new Mark();
                $mark->setMark(mt_rand(1,5))
                    ->setUser($users[mt_rand(0,count($users)-1)])
                    ->setRecipe($recipe);

                $manager->persist($mark);
            }

            }


            //Contact

            for ($c=0; $c < 5 ; $c++) { 
                $contact = new Contact();
                $contact->setFullName($this->faker->name)
                    ->setEmail($this->faker->email)
                    ->setSubject('Demande n°'.($c+1))
                    ->setMessage($this->faker->text(200));
                    
                    $manager->persist($contact);
                
            }
        

            $manager->flush();
        }
}
