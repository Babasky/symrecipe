<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('plainPassword',RepeatedType::class,[
            'type'=>PasswordType::class,
            'first_options'=>[
                'label'=>'Mot de passe',
                'attr'=>['class'=>'form-control'],
                'label_attr'=>['class'=>'form-label mt-2'],
            ],
            'second_options'=>[
                'label'=>'Confirmation du mot de passe',
                'attr'=>['class'=>'form-control'],
                'label_attr'=>['class'=>'form-label mt-2'],
            ],
            
            'invalid_message'=>'Les mots de passe ne correspondent pas',
            
            'attr'=>[
                'class'=>'form-control',
            ],
            'label'=>'Mot de passe',
            'label_attr'=>[
                'class'=>'form-label mt-2'
            ],
            'constraints'=>[
                new Assert\NotBlank(),
            ]
        ])
        ->add('newPassword', PasswordType::class,[
            'attr'=>['class'=>'form-control'],
            'label'=>'Nouveau mot de passe',
            'label_attr'=>['class'=>'form-label mt-2'],
            'constraints'=>[
                new Assert\NotBlank(),
            ]
        ])
        
        ->add('submit',SubmitType::class,[
            'attr'=>[
                'class'=>'btn btn-dark mt-2'
            ],
            'label'=>'Réinitialiser',


        ]);
    
    
    }

}