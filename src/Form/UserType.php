<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('fullName',TextType::class,[
            'attr'=>[
                'class'=>'form-control',
                'minlength'=>'10',
                'maxlength'=>'150'
            ],
            'label'=>'Non/Prénom',
            'label_attr'=>[
                'class'=>'form-label mt-2',
            ],
            'constraints'=>[
                new Assert\NotNull(),
                new Assert\Length(['max'=>50]),
            ],
        ])
        ->add('pseudo',TextType::class,[
            'attr'=>[
                'class'=>'form-control',
                'maxlength'=>'50'
            ],
            'required'=>false,
            'label'=>'Pseudonyme(Facultatif)',
            'label_attr'=>[
                'class'=>'form-label mt-2'
            ],
            'constraints'=>[
                new Assert\Length(['max'=>50]),
            ]
        ])

        ->add('plainPassword',PasswordType::class,[
            'attr'=>[
                'class'=>'form-control'
            ],
            'label'=>'Mot de passe',
            'label_attr'=>[
                'class'=>'form-label mt-2'
            ]
        ])
        
        ->add('submit',SubmitType::class,[
            'attr'=>[
                'class'=>'btn btn-dark mt-2'
            ],
            'label'=>'Enregistrer'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
